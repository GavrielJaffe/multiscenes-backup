from cognata_api.web_api.cognata_demo import CognataRequests as cog_api
from Processing.RunningScenario import Run
from Processing.DownloadingScenarios import Download
from Processing.WorkBook import CreateWorkbook
from Processing.WorkBookUniqueColumns import AddUniqueColumns as AUC
from Utils.DataFolder import DataFolder
from Utils import GenericFunctions, Paths, Bridge
import json, sys, os
import random

INPUT_FILE = "generated_csv.csv"
ENDING_LAT = 0
ENDING_LNG = 0
#------------------ API UTILS-----------------
def connect_to_backend(username, password, company_id):
    backendIP = f"https://{company_id}-api.cognata-studio.com/v1"
    # Create api connection to studio cloud instance
    # connects through API url
    api_connection = cog_api(backendIP, username, password)
    if not api_connection.is_logged_in:
        raise ValueError("illegal details")
    return api_connection

def get_all_scenarios(api_connection):
    return api_connection._perform_get_request("{api_url}/scenarios".format(api_url=api_connection.client_api_url))

            #"cognataAI" , openScenario
def start_simulation(api_connection, scenario_formula, client = "stats_algo_v1:1.19.0", driver_mode ="cognataAI"):
    return api_connection.generate_simulation(scenario_formula, client, annotate=False ,driver_mode=driver_mode)

def get_scenario_formula(api_connection, scenario_id):
    return api_connection._perform_get_request(
        "{api_url}/scenarios/{scene_id}".format(api_url=api_connection.client_api_url, scene_id=scenario_id))
    
#------------------- SEPERATE FUNCTIONS -----------------------------
def createSingleObject(name, id, lat = 31.84773578204212, lng = 34.68944638967515, type = 'BARREL12'):
    randomized = randomize(1, ["Lane", "LaneBoundary"])
    result = {
        "obj_type": "movingObjects",
        "name": name,
        "id": id,
        "waypoints": createDefaultWaypointList(id, randomized[0][1], randomized[0][0]),
        "scripts": createDefaultScriptList(id),
        "type": type, 
        "iconType": "building",
        "isEgoCar": False,
        "sockets": [],
        "totalDistance": "0.00 m"
        }
    return result

def createDefaultWaypointList(objectId, lat, lng):
    waypoints = [
        {
            'isAnchor': False, 
            'movingObjectId': objectId, 
            'position': {
                'lat': lat, 
                'lng': lng
            }
        }
    ]
    return waypoints

def createDefaultScriptList(objectId):
    scripts = [
                {
                    'readOnly': True, 
                    'triggerType': 'wayPointTrigger', 
                    'triggerValue': 0, 
                    'actionType': 'setPathSpeed', 
                    'actionValue': 1, 
                    'movingObjectId': objectId
                }, 
                {
                    'readOnly': True, 
                    'triggerType': 'wayPointTrigger', 
                    'triggerValue': 0, 
                    'actionType': 'setPathAcceleration', 
                    'actionValue': 0, 
                    'movingObjectId': objectId
                }, 
                {
                    'readOnly': True, 
                    'triggerType': 'wayPointTrigger', 
                    'triggerValue': 0, 
                    'actionType': 'setOrientation', 
                    'actionValue': 0, 
                    'movingObjectId': objectId
                }]
    return scripts

def createAList(amount):
    list = []
    for i in range(amount):
        list.append(createSingleObject(f"MovingObject{i}", f"moving_object{i}"))
    return list

def numberOfMovingObjects():
    while True:
        try:
            # we can make random in the amount and send back diffrent numbers avery time 
            movingObjects_number = random.randint(0, 9)	
            print('movingObjects_number -random :', movingObjects_number)
            # movingObjects_number = int(input("Enter the amount of objects in integer number only: "))
            break
        except ValueError:
            print("Please input integer only...")  
            continue
    return movingObjects_number

def randomize(n, types):
    lines = [line[:line.rindex(",")] for line in open(INPUT_FILE, "r").readlines() \
                                    if line.split(',')[-1].replace("\n", "") in types]
    return [list(map(\
        float, lines.pop(lines.index(random.choice(lines))).split(',')\
                                )) for times in range(n) if lines != []]
#--------------------------------------------------------------
def simulations_numbers(create_simulation_number):
    api = connect_to_backend("gavriel", "Aa123456", "2a4ab8d2")
    rand = randomize(1, "Lane")[0]
    ENDING_LAT = rand[1]
    ENDING_LNG = rand[0]
    for i in range(create_simulation_number):
        amount = numberOfMovingObjects()
        formula = {
        '_id': '635e83789d094a002fa7dd0c', 
        'name': 'multi scene test', 
        'description': 'Test scenario from API', 
        'useVehiclesTightBoundingBoxes': True, 
        'terrain': {
            'id': 'ASHDODV2',
            'description': 'Ashdod with some slight improvements and fixes.',
            'name': 'Ashdod V2.1', 'excludedSceneClasses': [], 
            'modes': [
                {
                    'label': 'Normal', 
                    'suffix': ''
                }
                ], 
            'mode': ''
            },
        'ego_car': {
            'car_physics_preset_sku': 'M1165HUM1', 
            'ego_car_sku': 'ODSUSKEN1', 
            'initial_speed': 0, 
            'desired_speed': 3.6, 
            'politeness': 20, 
            'time_to_collision': 1.5, 
            'distance_to_collision': 10, 
            'comfortable_braking': 2, 
            'lane_change_speed': 0.25, 
            'gear_mode': 0, 
            'starting_point': {
                'lat': 31.848672204405975,#randomize(1, "Lane")[0][1],
                'lng': 34.6875473856926, #randomize(1, "Lane")[0][0], # randomize(1, "Lane")[0][0]
                'roadId': '7', 
                'firstSectionID': 0, 
                'lastSectionID': 0, 
                'segmentId': '7_0_0_-1', 
                'lane': -1, 
                'isRelative': False, 
                'relativeDistance': 0, 
                'headway': 0, 
                'laneOffset': 0, 
                'anchorRef': None
                }, 
            'scripts': [
                {
                'id': 'ego_car_script_0', 
                'triggerType': 
                'relativeLocationTrigger', 
                'actionType': 'requestEndOfSimulation', 
                'anchorRef': {
                    'name': 'EgoCar end', 
                    'id': 'EgoCar_end', 
                    'lat': ENDING_LAT, 
                    'lng': ENDING_LNG, 
                    'isDraggable': False
                    }, 
                'reason': 'Reached end point', 
                'relativeDistance': 0, 
                'laneOffset': 0, 
                'headway': 0
                },
                {
                    'id': 'ego_car_script_1', 
                    'triggerType': 'simulationTimeTrigger', 
                    'actionType': 'setDestination', 
                    'simulationTimeTrigger': 0, 
                    'anchorRefAction': {
                        'name': 'EgoCar end', 
                        'id': 'EgoCar_end', 
                        'lat': ENDING_LAT, 
                        'lng': ENDING_LNG, 
                        'isDraggable': False
                        }
                }
                ], 
            'ending_point': {
                'lat': 31.848674482794284,#ENDING_LAT,
                'lng': 34.68800470232964,#ENDING_LNG, 
                'roadId': '7', 
                'firstSectionID': 0, 
                'lastSectionID': 0, 
                'segmentId': '7_0_0_-1', 
                'lane': -1
                }
            }, 
        'cartesian_params': {
            'variables': [], 
            'road_markings': ['No wear'], 
            'terrain_conditions': ['Dry'], 
            'weather_conditions': ['clear'], 
            'time_of_day': ['morning'], 
            'seed': ['6789'], 
            'temperature': [25]
            }, 
        'analysis_rules': [], 
        'dynamic_analysis_rules': [], 
        'spawn_objects': [], 
        'simulation_termination': {
            'timeout': 60, 
            'distance': -1
            }, 
        'traffic_localisation': 'COGNATA',
        'permutations': 1, 
        'sku': 'NEWSCENA46', 
        'scenarioType': 'cognataScenario', 
        'modification_time': '', 
        'creation_time': '', 
        'read_only': False, 
        'userCustomAnchors': [], 
        'intersections': [], 
        'tags': [], 
        'scenarioMessages': [], 
        'variables': [], 
        'dynamic_objects': [], 
        'movingObjects': createAList(amount), 
        'strict_spawn': True
    }
        start_simulation(api, formula ,driver_mode = "cognataAI")

def get_simulation_amount():
    while True:
        try:
            create_simulation_number = int(input("Enter the amount of simulation to create in integer number only: "))
            break
        except ValueError:
            print("Please input integer only...")  
            continue
    return create_simulation_number

# def generate_simulation(self, scenario_data, client_driver_version='', cognata_engine_version='',
#                             annotate=False, visibility_report=False, driver_mode=None, running_priority=10)

# ----------[FINAL FUNCTION]-------------
def main():
    create_simulation_number = get_simulation_amount()
    simulations_numbers(create_simulation_number)
    # a = randomize(1, "Lane")
    # print(a)
    
if __name__ == "__main__":
    main()
